# SectRect

Solution to the Intersection Rectangle Problem.

## Requirements
- `-std=c++14`

## Build
```
git clone https://gitlab.com/jasonleethomas/sectrect/tree/master
cd sectrect
make
```

## Usage
```
./sectrect [filename]
```

## Solution Files

- `sectrect.h`
- `sectrect.cpp`
- `main.cpp`
