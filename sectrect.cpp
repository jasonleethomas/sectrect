/** @file sectrect.cpp
 * Contains implementations of sectrect namespace
 */

#include <algorithm>
#include <cassert>
#include <fstream>
#include <set>
#include <stack>
#include <vector>
#include <utility>

#include "sectrect.h"
#include "json/json.h"

namespace sectrect {
    Json::Value 
    read_json(std::string filename) {    
        std::ifstream file(filename);
        Json::Value input;

        if (file.is_open()) {
            file >> input;
        }

        return input;
    }

    std::vector<Rectangle> 
    parse_rectangles(Json::Value input) {
        std::vector<Rectangle> rectangles;

        auto id = 0;
        for (auto rectangle : input["rects"]) {
            auto x = rectangle["x"].asInt();
            auto y = rectangle["y"].asInt();
            auto w = rectangle["w"].asInt();
            auto h = rectangle["h"].asInt();

            rectangles.emplace_back(id++, x, y, w, h );
        }

        return rectangles;
    }

    std::pair<std::vector<Edge>, std::vector<Edge>>
    split_rectangles(std::vector<Rectangle> rectangles) {
        std::vector<Edge> horizontal, vertical;
        for (auto rectangle : rectangles) {
            horizontal.emplace_back(rectangle.id, rectangle.x, Edge::begin);
            horizontal.emplace_back(rectangle.id, rectangle.x + rectangle.w, Edge::end);

            vertical.emplace_back(rectangle.id, rectangle.y, Edge::begin);
            vertical.emplace_back(rectangle.id, rectangle.y + rectangle.h, Edge::end);
        }

        return std::make_pair(horizontal, vertical);
    }

    std::set<Intersection> 
    find_intersections(std::pair<std::vector<Edge>, std::vector<Edge>> edges) {
        auto sort_by_value = [](auto edges) {
            std::sort(std::begin(edges), std::end(edges), [](auto a, auto b) {
                return a.value < b.value;
            });

            return edges;
        };

        auto horizontal_edges = sort_by_value(edges.first);
        auto vertical_edges = sort_by_value(edges.second);

        auto is_active = [](auto set, auto edge) {
            return set.find(edge.rectangle_id) != std::end(set);
        };

        auto is_another = [](auto a, auto b) {
            return a.rectangle_id != b.rectangle_id;
        };

        // perform vertical then horizontal sweeps, forming intersection sets
        // with respective rectangle ids.

        Intersection active_rectangles;
        std::set<Intersection> intersections;
        
        for (auto vertical : vertical_edges) {
            switch(vertical.type) {
            case Edge::begin:
                active_rectangles.insert(vertical.rectangle_id);
                if (active_rectangles.size() > 2) { // intersecting group
                    intersections.emplace(std::begin(active_rectangles), 
                                          std::end(active_rectangles));
                }
                for (auto horizontal : horizontal_edges) {
                    if (is_active(active_rectangles, horizontal) && 
                        is_another(horizontal, vertical)) { // intersecting pairs
                        intersections.insert(Intersection {{ horizontal.rectangle_id, 
                                                             vertical.rectangle_id }});
                    }
                }
                break;
            case Edge::end:
                active_rectangles.erase(vertical.rectangle_id);
                break;
            }
        }

        return intersections;
    }

    std::vector<std::pair<Intersection, Rectangle>>
    find_intersection_rectangles(std::set<Intersection> intersections, 
                                 std::vector<Rectangle> rectangles) {
                                     
        // find minumum and maximum rectangles in intersection set
        // according to relation defined by compare.
        
        auto lower_bound = [rectangles](auto intersection, auto compare) {
             auto lower_it = std::min_element(std::begin(intersection), 
                                              std::end(intersection), 
                                              compare);
             return rectangles[*lower_it];
        };

        auto upper_bound = [rectangles](auto intersection, auto compare) {
             auto upper_it = std::max_element(std::begin(intersection), 
                                              std::end(intersection), 
                                              compare);
             return rectangles[*upper_it];
        };

        // search for edges and the respective rectangle with the tightest 
        // bound on the intersection. i.e. left side closest to the right.
        // right side closest to the left. lowest ceiling, and highest floor. 
        
        auto left_rectangle = [upper_bound, rectangles](auto intersection) {
            return upper_bound(intersection, [rectangles](auto a, auto b) {
                return rectangles[a].x < rectangles[b].x;
            });
        };

        auto right_rectangle = [lower_bound, rectangles](auto intersection) {
            return lower_bound(intersection, [rectangles](auto a, auto b) {
                return (rectangles[a].x + rectangles[a].w) <
                       (rectangles[b].x + rectangles[b].w); 
            });
        };

        auto top_rectangle = [upper_bound, rectangles](auto intersection) {
            return upper_bound(intersection, [rectangles](auto a, auto b) {
                return rectangles[a].y < rectangles[b].y;
            });
        };

        auto bottom_rectangle = [lower_bound, rectangles](auto intersection) {
            return lower_bound(intersection, [rectangles](auto a, auto b) {
                return (rectangles[a].y + rectangles[a].h) <
                       (rectangles[b].y + rectangles[b].h); 
            });
        };
        
        // construct intersection rectangles from their bounding edges

        std::vector<std::pair<Intersection, Rectangle>> intersection_rectangles;

        auto id = rectangles.size();
        for (auto intersection : intersections) {

            auto left = left_rectangle(intersection);
            auto right = right_rectangle(intersection);
            auto top = top_rectangle(intersection);
            auto bottom = bottom_rectangle(intersection);

            auto x = left.x;
            auto y = top.y;

            // width and height of inner rectangle from difference
            auto w = right.x + right.w - left.x;
            auto h = bottom.y + bottom.h - top.y;

            intersection_rectangles.emplace_back(intersection, Rectangle { id++, x, y, w, h });
        }

        return intersection_rectangles;
    }

    template<typename Rectangle>
    bool intersects(Rectangle a, Rectangle b) {
        /*
         * We can verify these intersections using a more traditional
         * pair-wise approach by assuming the following:
         * Two rectangles A & B may intersect iff A's horizontal edges are 
         * not both outside B's horizontal edges. And A's horizontal edges 
         * are not both to the outside B's horizontal edges.  
         * Simply put, at least one of A's edges must be within B.
         */
        auto is_outside = [](auto p, auto q, auto dq) {
            return q + dq < p; 
        };

        auto x_is_outside = is_outside(a.x, b.x, b.w) || is_outside(b.x, a.x, a.w);
        auto y_is_outside = is_outside(a.y, b.y, b.h) || is_outside(b.y, a.y, a.h);

        auto intersection = true;
        if (x_is_outside || y_is_outside)
            intersection = false;

        return intersection;
    }

    void all_intersections_valid(std::set<Intersection> intersections, 
                                 std::vector<Rectangle> rectangles) {
        // for all sets, all rectangles in each set intersect
        for (auto intersection : intersections) {
            for (auto a : intersection) {
                for (auto b : intersection) {
                    assert(intersects(rectangles[a], rectangles[b]));
                 }
            }
        }
    }

    void all_intersections_found(std::set<Intersection> intersections, 
                                 std::vector<Rectangle> rectangles) {
        auto in_set = [](auto set, auto key) {
            return set.find(key) != std::end(set);
        };

        // for all rectangles, all intersections are in set
        for (auto a : rectangles) {
            for (auto b : rectangles) {
                if (a.id != b.id && intersects(a, b)) {
                    assert(in_set(intersections, Intersection {{ a.id, b.id }}));
                }
            }
        }
    }
}
