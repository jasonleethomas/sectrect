#pragma once

#include <algorithm>
#include <fstream>
#include <set>
#include <stack>
#include <vector>
#include <utility>

#include "json/json.h"

namespace sectrect {
    /**
     * Intersections are represented by a set of integers,
     * being ids of the rectangles intersecting.
     */
    using Intersection = std::set<size_t>;

    /**
     * Rectangle representing the input data. It is 
     * assumed that the rectangle is axis-aligned.
     * @var id Rectangle
     * @var x x position (right increasing)
     * @var y y position (downward increasing)
     * @var w width of rectangle
     * @var h height of rectangle
     */
    struct Rectangle {
        size_t const id;
        int const x;
        int const y;
        int const w;
        int const h;

        Rectangle(size_t id, int x, int y, int w, int h) :
            id(id),
            x(x),
            y(y),
            w(w),
            h(h)
        {}
    };

    /**
     * Edge object representing the rectangles horizontal and vertical edges.
     * @var value The edge's x or y value 
     * @var rectangle_id id of associated rectangle
     * @var type begin or end edge whne aligned with axis. 
     */
    struct Edge {
        enum Type { begin, end }; 

        size_t rectangle_id;
        int value;
        Type type;

        Edge(size_t rectangle_id, int value, Type type) :
            rectangle_id(rectangle_id),
            value(value),
            type(type)
        {}
    };

    /**
     * @brief Opens file stream of @p filename
     * @return Json::Value object with formatted contents
     * of @filename.
     */
    Json::Value 
    read_json(std::string filename);    

    /**
     * @brief Creates vector of Rectangles from JSON object @p input. 
     * Accesses x, y, w, h keys of JSON object by name.
     * If key value doesnt exist or is null, associated rectangle member
     * is initialized to zero. 
     * Each rectangle object is tagged with an id in the order it appears
     * in JSON array.
     * @return vector of Rectangle objects with values from JSON array.
     */
    std::vector<Rectangle> 
    parse_rectangles(Json::Value input);

    /**
     * @brief Separates @p rectangles into lists of its
     * horizontal and vertical edges.
     * Left & Top edges are considered the  horizontal and vertical 
     * begining of the rectangle respectively, whilst Right & Bottom
     * edges are considered the end of the rectangle on each axis.
     * @return std::pair<std::vector<Edge>, std::vector<Edge>>
     * containing lists of edges of each rectangle in @p rectangles.
     */
    std::pair<std::vector<Edge>, std::vector<Edge>>
    split_rectangles(std::vector<Rectangle> rectangles);

    /**
     * @brief Performs a Line-Sweep (or Plane Sweep) identifying the intersections
     * of the segments represented by the horizontal and vertical edges in @p edges.
     * Methods makes use of a std::set to facilitate O(log N) lookups, for an
     * overall O(N log N) implementation.
     * @sa https://en.wikipedia.org/wiki/Line_segment_intersection
     * @return std::set<Intersection> object contain sets of rectangle ids 
     * that have intersected.
     */
    std::set<Intersection> 
    find_intersections(std::pair<std::vector<Edge>, std::vector<Edge>> edges);

    /**
     * @brief identifies the rectangles formed by the set of intersections.
     * Each intersection rectangle is determined by finding the rectangle
     * whose edge is the tightest bound of the intersection area. 
     * @p returns a pair of intersections and its associated rectangle
     */
    std::vector<std::pair<Intersection, Rectangle>>
    find_intersection_rectangles(std::set<Intersection> intersections, 
                                 std::vector<Rectangle> rectangles);

    /**
     * @brief Performs a pair-wise verification of the intersections in the set associated
     * with @p rectangles. It verifies that all intersections found are valid.
     */
    void all_intersections_valid(std::set<Intersection> intersections, std::vector<Rectangle> rectangles);

    /**
     * @brief Performs a pair-wise test of all rectangles, verifying that all intersections were found
     * and exist in the set.
     */
    void all_intersections_found(std::set<Intersection> intersections, std::vector<Rectangle> rectangles);
}
