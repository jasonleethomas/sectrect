cc = g++ -std=c++14 -Wall -Wextra

sectrect: force
	$(cc) main.cpp sectrect.cpp jsoncpp.cpp -o sectrect

force:

clean:
	find . -name '*.sw*' -delete
