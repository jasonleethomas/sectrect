#include <iostream>

#include "sectrect.h"

using namespace sectrect;

int main (int argc, char ** argv) {
    std::string filename("rect.json");
    if (argc > 1) {
        filename = argv[1];
    }

    /// Work Work Work
    auto rectangles = parse_rectangles(read_json(filename));
    auto intersections = find_intersections(split_rectangles(rectangles));
    auto intersection_rectangles = find_intersection_rectangles(intersections, 
                                                                rectangles);
    /// Unit tests
    all_intersections_valid(intersections, rectangles);
    all_intersections_found(intersections, rectangles);
    
    auto print_rectangle = [](auto & stream, auto rectangle) -> decltype(stream) {
        stream << "id:" << rectangle.id + 1 << " "
               << "(x:" << rectangle.x << ", y:" << rectangle.y << ") "
               << "(w:" << rectangle.w << ", h:" << rectangle.h << ")\n";

        return stream;
    };

    /// Console Output
    std::cout << "Input:\n";
    for (auto rectangle : rectangles) {
        print_rectangle(std::cout, rectangle);
    }

    std::cout << "\nIntersections:\n";
    for (auto sectrect : intersection_rectangles) {
        auto sect = sectrect.first;

        std::cout << "Between rectangles: ";
        for (auto rectangle_id : sect) {
            std::cout << rectangle_id + 1 << " ";
        }

        auto rect = sectrect.second;

        std::cout << "forming rectangle ";
        print_rectangle(std::cout, rect);
    }

    return 0;
}
